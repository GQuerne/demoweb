package repository;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demoWeb.DemoWebApplication;
import com.example.demoWeb.ProductRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = DemoWebApplication.class)
public class ProductRepositoryTest {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(productRepository.findAll()).hasSize(1);
	}
	
	@Test
	public void testFindByNameAndPrice_andNoneFound() {
		assertThat(productRepository.findByNameAndPrice("android", 10)).isNull();
	}
	
	@Test
	public void testFindByNameAndPrice_andFoundOne() {
		assertThat(productRepository.findByNameAndPrice("iphone", 999)).isNotNull();
	}

}
