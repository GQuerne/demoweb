package repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Product;
import util.Context;

public class DAOProduct {
	
	public void insert(Product p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		em.persist(p);

		em.getTransaction().commit();
		em.close(); Context.destroy();

	}
	
	public Product findById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		Product p = em.find(Product.class, id);
		
		em.close(); Context.destroy();
		return p;
	}
	
	public List<Product> findAll() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		
		Query query = em.createQuery("SELECT p FROM Product p");
		List<Product> films = query.getResultList();
		
		em.close(); Context.destroy();
		return films;
	}
	
	public void update(Product p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		em.merge(p);

		em.getTransaction().commit();
		em.close(); Context.destroy();

	}
	
	public void delete(Product p) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

		em.getTransaction().begin();

		em.remove(em.merge(p));

		em.getTransaction().commit();
		em.close(); Context.destroy();

	}

}
