package com.example.demoWeb;

import model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ProductRepository extends JpaRepository<Product, Long> {

	Product findByNameAndPrice(
			@Param("name") String name,
			@Param("price") double price
			);

}
