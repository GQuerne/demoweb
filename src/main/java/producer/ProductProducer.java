package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import model.Product;
import service.ProductService;

@Component
public class ProductProducer {
	
	private Log logger = LogFactory.getLog(ProductProducer.class);
	
	private ProductService productService;
	
	@Autowired
	public ProductProducer(ProductService productService) {
		this.productService = productService;
	}
	
	@PostConstruct
	public void produceData() {
		findProducts();
		addOneProduct();
		findProducts();
	}
	
	private void addOneProduct() {
		logger.info("-> Adding new product now!");
		productService.addProduct(new Product("iphone", 99));
	}
	
	private void findProducts() {
		logger.info("Trying to find all products");
		List<Product> allProducts = productService.getAllProducts();
		if (allProducts.isEmpty()) {
			logger.info("No product found");
		} else {
			for (Product foundProduct : allProducts) {
				logger.info(String.format("product with id %d and name %s found", foundProduct.getId(), foundProduct.getName()));
			}
		}
	}

}
